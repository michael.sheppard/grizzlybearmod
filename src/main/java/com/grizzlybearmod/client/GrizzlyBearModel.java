/*
 * GrizzlyBearModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.grizzlybearmod.client;

import com.grizzlybearmod.common.GrizzlyBearEntity;
import net.minecraft.client.renderer.entity.model.QuadrupedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;


@OnlyIn(Dist.CLIENT)
public class GrizzlyBearModel<T extends GrizzlyBearEntity> extends QuadrupedModel<T> {

    public GrizzlyBearModel() {
        super(12, 0.0F, true, 16.0F, 4.0F, 2.25F, 2.0F, 24);
        textureWidth = 128;
        textureHeight = 64;
        headModel = new ModelRenderer(this, 0, 0);
        headModel.addBox(-3.5F, -3.0F, -3.0F, 7.0F, 7.0F, 7.0F, 0.0F);
        headModel.setRotationPoint(0.0F, 10.0F, -16.0F);
        headModel.setTextureOffset(0, 44).addBox(-2.5F, 1.0F, -6.0F, 5.0F, 3.0F, 3.0F, 0.0F);
        headModel.setTextureOffset(26, 0).addBox(-4.5F, -4.0F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F);
        ModelRenderer modelrenderer = headModel.setTextureOffset(26, 0);
        modelrenderer.mirror = true;
        modelrenderer.addBox(2.5F, -4.0F, -1.0F, 2.0F, 2.0F, 1.0F, 0.0F);
        body = new ModelRenderer(this);
        body.setTextureOffset(0, 19).addBox(-5.0F, -13.0F, -7.0F, 14.0F, 14.0F, 11.0F, 0.0F);
        body.setTextureOffset(39, 0).addBox(-4.0F, -25.0F, -7.0F, 12.0F, 12.0F, 10.0F, 0.0F);
        body.setRotationPoint(-2.0F, 9.0F, 12.0F);
        legBackRight = new ModelRenderer(this, 50, 22);
        legBackRight.addBox(-2.0F, 0.0F, -2.0F, 4.0F, 10.0F, 8.0F, 0.0F);
        legBackRight.setRotationPoint(-3.5F, 14.0F, 6.0F);
        legBackLeft = new ModelRenderer(this, 50, 22);
        legBackLeft.addBox(-2.0F, 0.0F, -2.0F, 4.0F, 10.0F, 8.0F, 0.0F);
        legBackLeft.setRotationPoint(3.5F, 14.0F, 6.0F);
        legFrontRight = new ModelRenderer(this, 50, 40);
        legFrontRight.addBox(-2.0F, 0.0F, -2.0F, 4.0F, 10.0F, 6.0F, 0.0F);
        legFrontRight.setRotationPoint(-2.5F, 14.0F, -7.0F);
        legFrontLeft = new ModelRenderer(this, 50, 40);
        legFrontLeft.addBox(-2.0F, 0.0F, -2.0F, 4.0F, 10.0F, 6.0F, 0.0F);
        legFrontLeft.setRotationPoint(2.5F, 14.0F, -7.0F);
        --legBackRight.rotationPointX;
        ++legBackLeft.rotationPointX;
        legBackRight.rotationPointZ += 0.0F;
        legBackLeft.rotationPointZ += 0.0F;
        --legFrontRight.rotationPointX;
        ++legFrontLeft.rotationPointX;
        --legFrontRight.rotationPointZ;
        --legFrontLeft.rotationPointZ;
    }

    public void setRotationAngles(@Nonnull T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        super.setRotationAngles(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
        float f = ageInTicks - (float) entityIn.ticksExisted;
        float f1 = entityIn.getStandingAnimationScale(f);
        f1 = f1 * f1;
        float f2 = 1.0F - f1;
        body.rotateAngleX = ((float) Math.PI / 2F) - f1 * (float) Math.PI * 0.35F;
        body.rotationPointY = 9.0F * f2 + 11.0F * f1;
        legFrontRight.rotationPointY = 14.0F * f2 - 6.0F * f1;
        legFrontRight.rotationPointZ = -8.0F * f2 - 4.0F * f1;
        legFrontRight.rotateAngleX -= f1 * (float) Math.PI * 0.45F;
        legFrontLeft.rotationPointY = legFrontRight.rotationPointY;
        legFrontLeft.rotationPointZ = legFrontRight.rotationPointZ;
        legFrontLeft.rotateAngleX -= f1 * (float) Math.PI * 0.45F;
        if (isChild) {
            headModel.rotationPointY = 10.0F * f2 - 9.0F * f1;
            headModel.rotationPointZ = -16.0F * f2 - 7.0F * f1;
        } else {
            headModel.rotationPointY = 10.0F * f2 - 14.0F * f1;
            headModel.rotationPointZ = -16.0F * f2 - 3.0F * f1;
        }

        headModel.rotateAngleX += f1 * (float) Math.PI * 0.15F;
    }
}
