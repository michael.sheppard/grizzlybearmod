/*
 * GrizzlyBearRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.grizzlybearmod.client;

import com.grizzlybearmod.common.GrizzlyBearEntity;
import com.grizzlybearmod.common.GrizzlyBearMod;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;

@OnlyIn(Dist.CLIENT)
public class GrizzlyBearRenderer extends MobRenderer<GrizzlyBearEntity, GrizzlyBearModel<GrizzlyBearEntity>> {
    private static final ResourceLocation GRIZZLY_BEAR_TEXTURE = new ResourceLocation(GrizzlyBearMod.MODID, "textures/entity/bear/grizzlybear.png");
    private static final float SCALE_FACTOR = 1.4f;

    public GrizzlyBearRenderer(EntityRendererManager renderManagerIn) {
        super(renderManagerIn, new GrizzlyBearModel<>(), 0.9F);
    }

    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull GrizzlyBearEntity entity) {
        return GRIZZLY_BEAR_TEXTURE;
    }

    protected void preRenderCallback(@Nonnull GrizzlyBearEntity grizzlyBearEntity, MatrixStack matrixStack, float partialTickTime) {
        matrixStack.scale(SCALE_FACTOR, SCALE_FACTOR, SCALE_FACTOR);
        super.preRenderCallback(grizzlyBearEntity, matrixStack, partialTickTime);
    }
}
