/*
 * GrizzlyMod.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.grizzlybearmod.common;

import com.grizzlybearmod.client.GrizzlyBearRenderer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.world.biome.SwampBiome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

@SuppressWarnings("unused")
@Mod(GrizzlyBearMod.MODID)
public class GrizzlyBearMod {
    public static final String GRIZZLYBEAR_MOD_NAME = "grizzlybear";
    public static final String MODID = "grizzlybearmod";
    public static final String GRIZZLYBEAR_NAME = "grizzlybear";
    public static final String GRIZZLYBEAR_SPAWN_EGG = "grizzlybear_spawn_egg";

    private static final Logger LOGGER = LogManager.getLogger(GrizzlyBearMod.MODID);

    // List of always excluded biome types
    private static final List<Type> excludedBiomeTypes = new ArrayList<>(Arrays.asList(
            Type.END,
            Type.NETHER,
            Type.VOID,
            Type.OCEAN,
            Type.BEACH,
            Type.DRY
    ));

    public GrizzlyBearMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::commonSetup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientSetup);
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ConfigHandler.commonSpec);
        MinecraftForge.EVENT_BUS.register(this);
    }

    public void commonSetup(final FMLCommonSetupEvent event) {
        registerSpawns();
        MinecraftForge.EVENT_BUS.register(new SpawnCheck());
    }

    public void clientSetup(final FMLClientSetupEvent event) {
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.GRIZZLY, GrizzlyBearRenderer::new);
    }

    @Mod.EventBusSubscriber(modid = GrizzlyBearMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    @ObjectHolder(GrizzlyBearMod.MODID)
    public static class RegistryEvents {
        public static final EntityType<GrizzlyBearEntity> GRIZZLY = EntityType.Builder.create(GrizzlyBearEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(64).build(GrizzlyBearMod.MODID);

        @SubscribeEvent
        public static void onEntityRegistry(final RegistryEvent.Register<EntityType<?>> event) {
            event.getRegistry().registerAll(
                    setup(RegistryEvents.GRIZZLY, GrizzlyBearMod.GRIZZLYBEAR_NAME)
            );
        }

        @SubscribeEvent
        public static void onItemRegistry(final RegistryEvent.Register<Item> event) {
            event.getRegistry().registerAll(
                    setup(new SpawnEggItem(RegistryEvents.GRIZZLY, 0x8B4513, 0x8B5A2B, new Item.Properties().group(ItemGroup.MISC)), GrizzlyBearMod.GRIZZLYBEAR_SPAWN_EGG)
            );
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final String name) {
            return setup(entry, new ResourceLocation(GrizzlyBearMod.MODID, name));
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final ResourceLocation registryName) {
            entry.setRegistryName(registryName);
            return entry;
        }

    }

    private void registerSpawns() {
        Biome[] forestBiomes = getBiomes(Type.FOREST, Type.PLAINS, Type.CONIFEROUS, Type.MOUNTAIN);
        int spawnProb = ConfigHandler.CommonConfig.getBearSpawnProb();
        int minSpawn = ConfigHandler.CommonConfig.getMinSpawn();
        int maxSpawn = ConfigHandler.CommonConfig.getMaxSpawn();
        registerEntitySpawn(RegistryEvents.GRIZZLY, forestBiomes, spawnProb, minSpawn, maxSpawn);
    }

    private static Biome[] getBiomes(Type... types) {
        LinkedList<Biome> list = new LinkedList<>();
        Collection<Biome> biomes = ForgeRegistries.BIOMES.getValues();

        for (Biome biome : biomes) {
            Set<Type> bTypes = BiomeDictionary.getTypes(biome);

            if (excludeThisBiome(bTypes)) {
                continue;
            }
            // process remaining biomes
            for (Type t : types) {
                if (BiomeDictionary.hasType(biome, t)) {
                    if (!list.contains(biome)) {
                        list.add(biome);
                        getLogger().info("Adding: " + biome.getRegistryName() + " biome for spawning");
                    }
                }
            }
        }
        return list.toArray(new Biome[0]);
    }

    private static boolean excludeThisBiome(Set<Type> types) {
        boolean excludeBiome = false;
        for (Type ex : excludedBiomeTypes) {
            if (types.contains(ex)) {
                excludeBiome = true;
                break;
            }
        }
        return excludeBiome;
    }

    @SuppressWarnings("SameParameterValue")
    private static void registerEntitySpawn(EntityType<? extends LivingEntity> type, Biome[] biomes, int spawnProb, int minSpawn, int maxSpawn) {
        if (spawnProb <= 0) {
            return; // do not spawn this entity
        }

        for (Biome bgb : biomes) {
            Biome biome = ForgeRegistries.BIOMES.getValue(bgb.getRegistryName());
            if (biome != null) {
                if (biome instanceof SwampBiome) {
                    spawnProb = spawnProb + 10;
                }
                biome.getSpawns(EntityClassification.CREATURE).add(new Biome.SpawnListEntry(type, spawnProb, minSpawn, maxSpawn));
            }
        }
    }

    public static Logger getLogger() {
        return LOGGER;
    }

}
