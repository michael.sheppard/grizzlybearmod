/*
 * GrizzlyEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.grizzlybearmod.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.FoxEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class GrizzlyBearEntity extends AnimalEntity {
    private static final DataParameter<Boolean> IS_STANDING = EntityDataManager.createKey(GrizzlyBearEntity.class, DataSerializers.BOOLEAN);
    private float clientSideStandAnimation0;
    private float clientSideStandAnimation;
    private int warningSoundTicks;

    public GrizzlyBearEntity(EntityType<? extends GrizzlyBearEntity> entity, World world) {
        super(entity, world);
    }

    public AgeableEntity createChild(@Nonnull AgeableEntity ageable) {
        return GrizzlyBearMod.RegistryEvents.GRIZZLY.create(world);
    }

    public boolean isBreedingItem(@Nonnull ItemStack stack) {
        return false;
    }

    protected void registerGoals() {
        super.registerGoals();
        goalSelector.addGoal(0, new SwimGoal(this));
        goalSelector.addGoal(1, new GrizzlyBearEntity.MeleeAttackGoal());
        goalSelector.addGoal(1, new GrizzlyBearEntity.PanicGoal());
        goalSelector.addGoal(4, new FollowParentGoal(this, 1.25D));
        goalSelector.addGoal(5, new RandomWalkingGoal(this, 1.0D));
        goalSelector.addGoal(6, new LookAtGoal(this, PlayerEntity.class, 6.0F));
        goalSelector.addGoal(7, new LookRandomlyGoal(this));
        targetSelector.addGoal(1, new GrizzlyBearEntity.HurtByTargetGoal());
        targetSelector.addGoal(2, new GrizzlyBearEntity.AttackPlayerGoal());
        targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, FoxEntity.class, 10, true, true, null));
    }

    protected void registerAttributes() {
        super.registerAttributes();
        getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(30.0D);
        getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(20.0D);
        getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.25D);
        getAttributes().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE);
        getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(6.0D);
    }

    protected SoundEvent getAmbientSound() {
        return isChild() ? SoundEvents.ENTITY_POLAR_BEAR_AMBIENT_BABY : SoundEvents.ENTITY_POLAR_BEAR_AMBIENT;
    }

    protected SoundEvent getHurtSound(@Nonnull DamageSource damageSourceIn) {
        return SoundEvents.ENTITY_POLAR_BEAR_HURT;
    }

    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_POLAR_BEAR_DEATH;
    }

    protected void playStepSound(@Nonnull BlockPos pos, @Nonnull BlockState blockIn) {
        playSound(SoundEvents.ENTITY_POLAR_BEAR_STEP, 0.15F, 1.0F);
    }

    protected void playWarningSound() {
        if (warningSoundTicks <= 0) {
            playSound(SoundEvents.ENTITY_POLAR_BEAR_WARNING, 1.0F, getSoundPitch());
            warningSoundTicks = 40;
        }

    }

    protected void registerData() {
        super.registerData();
        dataManager.register(IS_STANDING, false);
    }

    public void tick() {
        super.tick();
        if (world.isRemote) {
            if (clientSideStandAnimation != clientSideStandAnimation0) {
                recalculateSize();
            }

            clientSideStandAnimation0 = clientSideStandAnimation;
            if (isStanding()) {
                clientSideStandAnimation = MathHelper.clamp(clientSideStandAnimation + 1.0F, 0.0F, 6.0F);
            } else {
                clientSideStandAnimation = MathHelper.clamp(clientSideStandAnimation - 1.0F, 0.0F, 6.0F);
            }
        }

        if (warningSoundTicks > 0) {
            --warningSoundTicks;
        }

    }

    @Nonnull
    public EntitySize getSize(@Nonnull Pose curPose) {
        if (clientSideStandAnimation > 0.0F) {
            float standHeight = clientSideStandAnimation / 6.0F;
            float fullHeight = 1.0F + standHeight;
            return super.getSize(curPose).scale(1.0F, fullHeight);
        } else {
            return super.getSize(curPose);
        }
    }

    public boolean attackEntityAsMob(Entity entityIn) {
        boolean flag = entityIn.attackEntityFrom(DamageSource.causeMobDamage(this), (float) ((int) getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getValue()));
        if (flag) {
            applyEnchantments(this, entityIn);
        }

        return flag;
    }

    public boolean isStanding() {
        return dataManager.get(IS_STANDING);
    }

    public void setStanding(boolean standing) {
        dataManager.set(IS_STANDING, standing);
    }

    @SuppressWarnings("unused")
    @OnlyIn(Dist.CLIENT)
    public float getStandingAnimationScale(float p_189795_1_) {
        return MathHelper.lerp(p_189795_1_, clientSideStandAnimation0, clientSideStandAnimation) / 6.0F;
    }

    protected float getWaterSlowDown() {
        return 0.98F;
    }

    public ILivingEntityData onInitialSpawn(@Nonnull IWorld worldIn, @Nonnull DifficultyInstance difficultyIn, @Nonnull SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag) {
        if (spawnDataIn == null) {
            spawnDataIn = new AgeableEntity.AgeableData();
            ((AgeableEntity.AgeableData) spawnDataIn).func_226258_a_(1.0F);
        }

        return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
    }

    class AttackPlayerGoal extends NearestAttackableTargetGoal<PlayerEntity> {
        public AttackPlayerGoal() {
            super(GrizzlyBearEntity.this, PlayerEntity.class, 20, true, true, null);
        }

        public boolean shouldExecute() {
            if (!GrizzlyBearEntity.this.isChild()) {
                if (super.shouldExecute()) {
                    for (GrizzlyBearEntity GrizzlyBearentity : GrizzlyBearEntity.this.world.getEntitiesWithinAABB(GrizzlyBearEntity.class, GrizzlyBearEntity.this.getBoundingBox().grow(8.0D, 4.0D, 8.0D))) {
                        if (GrizzlyBearentity.isChild()) {
                            return true;
                        }
                    }
                }

            }
            return false;
        }

        protected double getTargetDistance() {
            return super.getTargetDistance() * 0.5D;
        }
    }

    class HurtByTargetGoal extends net.minecraft.entity.ai.goal.HurtByTargetGoal {
        public HurtByTargetGoal() {
            super(GrizzlyBearEntity.this);
        }

        public void startExecuting() {
            super.startExecuting();
            if (GrizzlyBearEntity.this.isChild()) {
                alertOthers();
                resetTask();
            }

        }

        protected void setAttackTarget(@Nonnull MobEntity mobIn, @Nonnull LivingEntity targetIn) {
            if (mobIn instanceof GrizzlyBearEntity && !mobIn.isChild()) {
                super.setAttackTarget(mobIn, targetIn);
            }

        }
    }

    class MeleeAttackGoal extends net.minecraft.entity.ai.goal.MeleeAttackGoal {
        public MeleeAttackGoal() {
            super(GrizzlyBearEntity.this, 1.25D, true);
        }

        protected void checkAndPerformAttack(@Nonnull LivingEntity enemy, double distToEnemySqr) {
            double d0 = getAttackReachSqr(enemy);
            if (distToEnemySqr <= d0 && attackTick <= 0) {
                attackTick = 20;
                attacker.attackEntityAsMob(enemy);
                GrizzlyBearEntity.this.setStanding(false);
            } else if (distToEnemySqr <= d0 * 2.0D) {
                if (attackTick <= 0) {
                    GrizzlyBearEntity.this.setStanding(false);
                    attackTick = 20;
                }

                if (attackTick <= 10) {
                    GrizzlyBearEntity.this.setStanding(true);
                    GrizzlyBearEntity.this.playWarningSound();
                }
            } else {
                attackTick = 20;
                GrizzlyBearEntity.this.setStanding(false);
            }

        }

        public void resetTask() {
            GrizzlyBearEntity.this.setStanding(false);
            super.resetTask();
        }

        protected double getAttackReachSqr(LivingEntity attackTarget) {
            return 4.0F + attackTarget.getWidth();
        }
    }

    class PanicGoal extends net.minecraft.entity.ai.goal.PanicGoal {
        public PanicGoal() {
            super(GrizzlyBearEntity.this, 2.0D);
        }

        public boolean shouldExecute() {
            return (GrizzlyBearEntity.this.isChild() || GrizzlyBearEntity.this.isBurning()) && super.shouldExecute();
        }
    }

}
