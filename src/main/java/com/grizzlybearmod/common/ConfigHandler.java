/*
 * ConfigHander.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.grizzlybearmod.common;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Paths;

public class ConfigHandler {
    public static void loadConfig() {
        CommentedFileConfig.builder(Paths.get("config", GrizzlyBearMod.GRIZZLYBEAR_MOD_NAME, GrizzlyBearMod.MODID + ".toml")).build();
    }

    public static class CommonConfig {
        public static ForgeConfigSpec.IntValue bearSpawnProb;
        public static ForgeConfigSpec.IntValue minSpawn;
        public static ForgeConfigSpec.IntValue maxSpawn;
        public static ForgeConfigSpec.BooleanValue randomScale;

        public CommonConfig(ForgeConfigSpec.Builder builder) {
            builder.comment("Grizzly Bear Mod Config").push("CommonConfig");

            minSpawn = builder
                    .comment("Minimum number of bears to spawn at one time")
                    .translation("config.grizzlybearmod.minSpawn")
                    .defineInRange("minSpawn", 1, 1, 5);

            maxSpawn = builder
                    .comment("Maximum number of bears to spawn at one time")
                    .translation("config.grizzlybearmod.maxSpawn")
                    .defineInRange("maxSpawn", 2, 1, 8);

            bearSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.grizzlybearmod.bearSpawnProb")
                    .defineInRange("bearSpawnProb", 8, 0, 100);

            randomScale = builder
                    .comment("Set to false to disable random scaling of entities, default is true.")
                    .translation("config.grizzlybearmod.randomScale")
                    .define("randomScale", true);

            builder.pop();
        }

        public static int getBearSpawnProb() {
            return bearSpawnProb.get();
        }

        public static int getMinSpawn() {
            return minSpawn.get();
        }

        public static int getMaxSpawn() {
            return maxSpawn.get();
        }

        public static boolean useRandomScaling() {
            return randomScale.get();
        }
    }

    static final ForgeConfigSpec commonSpec;
    public static final CommonConfig COMMON_CONFIG;
    static {
        final Pair<CommonConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(CommonConfig::new);
        commonSpec = specPair.getRight();
        COMMON_CONFIG = specPair.getLeft();
    }
}
